import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TraderComponentComponent} from 'src/app/trader-component/trader-component.component';
import { AddTraderComponentComponent } from './add-trader-component/add-trader-component.component';
import {ObservableComponent} from './observable/observable.component';
import {ComponentsAndTemplatesComponentComponent} from './components-and-templates-component/components-and-templates-component.component';
import { InputComponent } from './input/input.component';
import { ParentComponent } from './ViewChild/parent/parent.component';
import { RouterComponent } from './router/router.component';
import { FakeComponent } from './fake/fake.component';

const routes: Routes = [
  {path: 'traders', component: TraderComponentComponent},
  {path: 'traders/addTrader', component: AddTraderComponentComponent},
  {path : 'traders/observable' , component : ObservableComponent},
  {path : 'traders/componentsAndTemplatesComponent' , component : ComponentsAndTemplatesComponentComponent},
  {path : 'traders/input' , component : InputComponent},
  {path : 'traders/parent' , component : ParentComponent},
  {path:'traders/router',component:RouterComponent},
  {path:'traders/router/fake',component:FakeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
