import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTraderComponentComponent } from './add-trader-component.component';

describe('AddTraderComponentComponent', () => {
  let component: AddTraderComponentComponent;
  let fixture: ComponentFixture<AddTraderComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddTraderComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTraderComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
