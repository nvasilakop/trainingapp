import { Component, OnInit, Input } from '@angular/core';
import { Trader } from '../Classes/Trader';
import {Traders} from '../Mock/TraderMock';
import { TradeServiceService } from '../TraderService/trade-service.service';
import {FormGroup, FormControl, Validators , EmailValidator} from '@angular/forms';

@Component({
  selector: 'app-add-trader-component',
  templateUrl: './add-trader-component.component.html',
  styleUrls: ['./add-trader-component.component.css']
})
export class AddTraderComponentComponent implements OnInit {

  trader: Trader;
  traderProfileForm: FormGroup;
  constructor() {
      this.trader = new Trader();
      //αν δεν μπει το παραπανω line τότε κάνει push ένα κενό object? Έτσι φαίνεται
      const emailRegEx = '^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$';

      this.traderProfileForm = new FormGroup({
        age : new FormControl(this.trader.age, [Validators.required]),
        name : new FormControl(this.trader.name),
        email : new FormControl(this.trader.email,[Validators.email,Validators.pattern(emailRegEx)])
      });
    }

  ngOnInit() {
  }

  addTrader(): void {
    Traders.push(this.trader);
  }

  addTrader2(): void {
    this.trader.age = this.traderProfileForm.value['age'] ;
    this.trader.name = this.traderProfileForm.value['name'] ;
    this.trader.email = this.traderProfileForm.value['email'];
    Traders.push(this.trader);
  }

}
