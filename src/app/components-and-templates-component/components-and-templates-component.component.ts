import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-components-and-templates-component',
  templateUrl: './components-and-templates-component.component.html',
  styleUrls: ['./components-and-templates-component.component.css']
})
export class ComponentsAndTemplatesComponentComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
