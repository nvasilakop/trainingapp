import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentsAndTemplatesComponentComponent } from './components-and-templates-component.component';

describe('ComponentsAndTemplatesComponentComponent', () => {
  let component: ComponentsAndTemplatesComponentComponent;
  let fixture: ComponentFixture<ComponentsAndTemplatesComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComponentsAndTemplatesComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentsAndTemplatesComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
