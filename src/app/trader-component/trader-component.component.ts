import { Component, OnInit } from '@angular/core';
import {TradeServiceService} from 'src/app/TraderService/trade-service.service';
import {Trader} from 'src/app/Classes/Trader';


@Component({
  selector: 'app-trader-component',
  templateUrl: './trader-component.component.html',
  styleUrls: ['./trader-component.component.css']
})
export class TraderComponentComponent implements OnInit {

 traders: Trader[];
 selectedTrader: Trader;
  constructor(private _service: TradeServiceService) { }



  ngOnInit() {
    // this.traders = this._service.getTraders();
  }

  onClick(trader: Trader): void {
    this.selectedTrader = trader;
}
getTraders(visible: boolean): void {
  let traderNew = new Trader();
  traderNew.age = 123;
  traderNew.name = 'EventEmitterName';
  traderNew.email = 'EventEmitterEmail';
  if (visible) {
  this.traders = this._service.getTraders();
  this.traders.push(traderNew);
  } else {
    this.traders = this._service.getTraders();
  }
}


}
