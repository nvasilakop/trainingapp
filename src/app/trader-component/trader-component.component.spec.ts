import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TraderComponentComponent } from './trader-component.component';

describe('TraderComponentComponent', () => {
  let component: TraderComponentComponent;
  let fixture: ComponentFixture<TraderComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TraderComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TraderComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
