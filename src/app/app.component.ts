import { Component,ViewRef,TemplateRef} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'TrainingApp';
 selected : boolean = false;
 constructor(){}
changeFlag(){
  this.selected = !this.selected;
}
}
