import { Component, OnInit , Input} from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {Trader} from 'src/app/Classes/Trader';

@Component({
  selector: 'app-update-trader',
  templateUrl: './update-trader.component.html',
  styleUrls: ['./update-trader.component.css']
})
export class UpdateTraderComponent implements OnInit {
  @Input() trader: Trader;
  updateTraderFormGroup: FormGroup;
  constructor() {
    this.trader = new Trader();
    this.updateTraderFormGroup = new FormGroup({
      age : new FormControl(this.trader.age),
      name : new FormControl(this.trader.name)
    });
   }
  ngOnInit() {
  }

  updateTrader() {
    this.trader.age = this.updateTraderFormGroup.value['age'];
    this.trader.name = this.updateTraderFormGroup.value['name'];
    this.updateTraderFormGroup.patchValue({name : this.trader.name});
  }
}
