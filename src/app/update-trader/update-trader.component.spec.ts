import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateTraderComponent } from './update-trader.component';

describe('UpdateTraderComponent', () => {
  let component: UpdateTraderComponent;
  let fixture: ComponentFixture<UpdateTraderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateTraderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateTraderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
