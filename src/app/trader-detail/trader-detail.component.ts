import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Trader } from '../Classes/Trader';
import {DeleteTraderComponent} from '../delete-trader/delete-trader.component';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-trader-detail',
  templateUrl: './trader-detail.component.html',
  styleUrls: ['./trader-detail.component.css']
})
export class TraderDetailComponent implements OnInit {

@Input() trader: Trader;
@Output() deleteRequest = new Observable<Trader>();
  constructor() { }

  ngOnInit() {
   
    // console.log(this.deleteRequest)
  }
  clickMe():void{
    this.deleteRequest.subscribe(r => {this.trader = r});
    // console.log(this.trader);
    console.log(this.deleteRequest)
  }

}
