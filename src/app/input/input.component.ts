import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css']
})
export class InputComponent implements OnInit {

  constructor() { }
  things :string[] = new Array<string>();

  ngOnInit() {
  }

  addThing(item : string):void{
    this.things.push(item);
    console.log(this.things);
  }
}
