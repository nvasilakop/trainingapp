import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TraderComponentComponent } from './trader-component/trader-component.component';
import { AddTraderComponentComponent } from './add-trader-component/add-trader-component.component';
import { TraderDetailComponent } from './trader-detail/trader-detail.component';
import { UpdateTraderComponent } from './update-trader/update-trader.component';
import { ObservableComponent } from './observable/observable.component';
import { ComponentsAndTemplatesComponentComponent } from './components-and-templates-component/components-and-templates-component.component';
import { DeleteTraderComponent } from './delete-trader/delete-trader.component';
import {PipeModule} from './pipe/pipe.module'; 
import { GenericInputModule } from './generic-input/generic-input.module';
import { InputComponent } from './input/input.component';
import { ParentComponent } from './ViewChild/parent/parent.component';
import { ChildComponent } from './ViewChild/child/child.component';
import { RouterComponent } from './router/router.component';
import { FakeComponent } from './fake/fake.component';

@NgModule({
  declarations: [
    AppComponent,
    TraderComponentComponent,
    AddTraderComponentComponent,
    TraderDetailComponent,
    UpdateTraderComponent,
    ObservableComponent,
    ComponentsAndTemplatesComponentComponent,
    DeleteTraderComponent,
    InputComponent,
    ParentComponent,
    ChildComponent,
    RouterComponent,
    FakeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    PipeModule,
    GenericInputModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
