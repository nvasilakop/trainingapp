import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TestPipePipe } from './test-pipe.pipe';

@NgModule({
  declarations: [TestPipePipe],
  imports: [
    CommonModule
  ],
  exports :[TestPipePipe]
})
export class PipeModule { }
