import { Component, OnInit,TemplateRef, ViewContainerRef, ViewChild, ContentChild, ViewChildren, QueryList, AfterViewInit, AfterContentInit, ContentChildren } from '@angular/core';
import { ChildComponent } from '../child/child.component';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent implements AfterViewInit,AfterContentInit {

  constructor() { }


  childs : ChildComponent[]= [];
  @ViewChild(ChildComponent) childComp : ChildComponent;
  @ViewChildren(ChildComponent) childComps : QueryList<ChildComponent>;

  @ContentChild(ChildComponent) childContent : ChildComponent;

  @ContentChildren(ChildComponent) childsCont : QueryList<ChildComponent>;
  
  ngAfterContentInit(): void {
    console.log(this.childContent);
    console.log(this.childsCont.toArray())
  }

  ngAfterViewInit(): void {
    this.childs.push(this.childComp);
    console.log("Two ways of view children");
    console.log(this.childs);
    console.log(this.childComps.toArray())
  }



  // removeParent(){
  //   this.viewRef.clear();
  // }
}
