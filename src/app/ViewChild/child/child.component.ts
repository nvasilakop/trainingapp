import { Component, OnInit } from '@angular/core';
import{Trader} from '../../Classes/Trader'; 

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {
trader :  Trader = <Trader>({name : "Nikos" , age :32, email :'nvasilakop@@fmail.xom', id :123456});
  constructor() { }

  ngOnInit() {
  }

}
