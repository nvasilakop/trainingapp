import { Injectable } from '@angular/core';
import {Traders} from 'src/app/Mock/TraderMock';
import { Trader } from '../Classes/Trader';

@Injectable({
  providedIn: 'root'
})
export class TradeServiceService {

  constructor() {
   }

  getTraders(): Trader[] {
    return Traders;
  }
}
