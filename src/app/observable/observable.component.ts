import { Component, OnInit , Output, EventEmitter} from '@angular/core';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-observable',
  templateUrl: './observable.component.html',
  styleUrls: ['./observable.component.css']
})

export class ObservableComponent implements OnInit {

  constructor() { }
  visible = true;
  @Output() open = new EventEmitter<any>();
  @Output() close = new EventEmitter<any>();

  ngOnInit() {
  }

  toggle() {
    this.visible = !this.visible;
    if (this.visible) {
      this.open.emit(this.visible);
      console.log(this.open);
    } else {
      this.close.emit(this.visible);
    }
  }
}
