import { Component, OnInit ,Input, Output, EventEmitter} from '@angular/core';
import {Trader} from '../Classes/Trader';
import {Traders} from '../Mock/TraderMock';

@Component({
  selector: 'app-delete-trader',
  templateUrl: './delete-trader.component.html',
  styleUrls: ['./delete-trader.component.css']
})
export class DeleteTraderComponent implements OnInit {

 trader: Trader;

  result: string;
  result2 : string;
  constructor() {   }

  ngOnInit() {
  }

  clickMe() : string{
    this.result = 'Deleted';
    return this.result;
  }

  deleteTrader(trader:Trader):void{
     this.trader = trader;
    this.result2 = 'Deleted2';
    console.log(this.trader);
    // return this.result2;
    // let smallTraders = Traders.filter(el => el.name != trader.name);
    // console.log(smallTraders);
  }
}
