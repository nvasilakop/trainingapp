import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteTraderComponent } from './delete-trader.component';

describe('DeleteTraderComponent', () => {
  let component: DeleteTraderComponent;
  let fixture: ComponentFixture<DeleteTraderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteTraderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteTraderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
